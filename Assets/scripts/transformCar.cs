﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class transformCar : MonoBehaviour
{

    public GameObject car;
    public static int curpoz=0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            car.transform.Translate(-5*Time.deltaTime,0,0);
            if (Input.GetKey(KeyCode.A))
            {
                car.transform.Rotate(0, -5, 0);
                curpoz -= 5;
            }
            if (Input.GetKey(KeyCode.D))
            {
                car.transform.Rotate(0, 5, 0);
                curpoz += 5;
            }
        }

        if (Input.GetKey(KeyCode.S))
        {
            car.transform.Translate(2 * Time.deltaTime, 0, 0);
            if (Input.GetKey(KeyCode.A))
            {
                car.transform.Rotate(0, -5, 0);
                curpoz -= 5;
            }
            if (Input.GetKey(KeyCode.D))
            {
                car.transform.Rotate(0, 5, 0);
                curpoz += 5;
            }
        }

    }
}
