﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wheelmoving : MonoBehaviour
{

    [SerializeField] GameObject wheel1, wheel2, wheel3, wheel4;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.W))
        {
            wheel1.transform.Rotate(0, 5, 0);
            wheel2.transform.Rotate(0, 5, 0);
            wheel3.transform.Rotate(0, 5, 0);
            wheel4.transform.Rotate(0, 5, 0);

        }
        if (Input.GetKey(KeyCode.S))
        {
            wheel1.transform.Rotate(0, -3, 0);
            wheel2.transform.Rotate(0, -3, 0);
            wheel3.transform.Rotate(0, -3, 0);
            wheel4.transform.Rotate(0, -3, 0);

        }
    }
}
